
$("#id_formulario").validate({
  rules: {
    "txtEmail": {
      required: true,
      email: true
    },
    "txtRut": {
      required: true
    },
    "txtNombre": {
      required: true
    },
    "txtMovil": {
      required: true,
      tel: true,
      minlength: 9
    },
    "txtFecha": {
      required: true
    },
    "txtCiudad" : {
      required: true
    }


  },

  messages: {
    "txtEmail": {
      required: 'Ingrese email',
      email: 'No cumple formato'
    },
    "txtRut": {
      required: 'Ingrese rut'
    },
    "txtNombre": {
      required: 'Ingrese nombre'
    },
    "txtMovil": {
      required: 'Ingrese telefono movil',
      tel: 'No cumple formato',
      minlength:'Teléfono no válido'
    },
    "txtFecha": {
      required: 'Ingrese fecha',
      max: 'La fecha debe ser anterior a 31/01/2001'
    },
    "txtCiudad": {
      required: 'Ingrese ciudad'
    }
  }
});

function alphaOnly(event) {
  var key = event.keyCode;`enter code here`
  return ((key >= 65 && key <= 90) || key == 8);
};

function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
};
